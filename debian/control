Source: ripe-atlas-sagan
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-setuptools,
               python3-all,
               python3-dateutil,
               python3-tz,
               python3-ipy,
               python3-cryptography,
               python3-pytest,
               python3-sphinx
Rules-Requires-Root: no
Standards-Version: 4.1.5
Testsuite: autopkgtest-pkg-pybuild
Homepage: https://github.com/RIPE-NCC/ripe.atlas.sagan
Vcs-Git: https://salsa.debian.org/python-team/packages/ripe-atlas-sagan.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/ripe-atlas-sagan

Package: python3-ripe-atlas-sagan
Architecture: all
Depends: python3-dateutil, python3-tz, python3-ipy,
 ${misc:Depends}, ${python3:Depends}
Description: Python3 library for parsing RIPE Atlas measurement results
 RIPE Atlas Sagan is a Python library for parsing RIPE Atlas measurement
 results. It aims at providing a consistent high-level interface for a rapidly
 changing low-level format used by the RIPE Atlas probes. RIPE Atlas is a
 global network of probes that measure Internet connectivity and reachability,
 providing an overview of the state of the Internet in real time.
 .
 This package contains the Python3 library.

Package: python-ripe-atlas-sagan-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Python library for parsing RIPE Atlas measurement results (documentation)
 RIPE Atlas Sagan is a Python library for parsing RIPE Atlas measurement
 results. It aims at providing a consistent high-level interface for a rapidly
 changing low-level format used by the RIPE Atlas probes. RIPE Atlas is a
 global network of probes that measure Internet connectivity and reachability,
 providing an overview of the state of the Internet in real time.
 .
 This package contains the library's documentation.
